import {
  mobile,
  backend,
  creator,
  web,
  javascript,
  typescript,
  html,
  css,
  reactjs,
  redux,
  tailwind,
  nodejs,
  mongodb,
  git,
  figma,
  docker,
  carrent,
  jobit,
  threejs,
  alivio,
  astics,
  acquaint,
} from "../assets";

export const navLinks = [
  {
    id: "about",
    title: "About",
  },
  {
    id: "work",
    title: "Work",
  },
  {
    id: "contact",
    title: "Contact",
  },
];

const services = [
  {
    title: "Web Developer",
    icon: web,
  },
  {
    title: "React Native Developer",
    icon: mobile,
  },
  {
    title: "Backend Developer",
    icon: backend,
  },
  {
    title: "Devops",
    icon: creator,
  },
];

const technologies = [
  {
    name: "HTML 5",
    icon: html,
  },
  {
    name: "CSS 3",
    icon: css,
  },
  {
    name: "JavaScript",
    icon: javascript,
  },
  {
    name: "TypeScript",
    icon: typescript,
  },
  {
    name: "React JS",
    icon: reactjs,
  },
  {
    name: "Redux Toolkit",
    icon: redux,
  },
  {
    name: "Tailwind CSS",
    icon: tailwind,
  },
  {
    name: "Node JS",
    icon: nodejs,
  },
  {
    name: "MongoDB",
    icon: mongodb,
  },
  {
    name: "Three JS",
    icon: threejs,
  },
  {
    name: "git",
    icon: git,
  },
  {
    name: "figma",
    icon: figma,
  },
  {
    name: "docker",
    icon: docker,
  },
];

const experiences = [
  {
    title: "AngularJS Nodejs Developer",
    company_name: "Alivio Technologies",
    icon: alivio,
    iconBg: "#383E56",
    date: "December 2017 - May 2018",
    points: [
      "Development on AngularJS, NodeJS with Database MySQL, MongoDB. Also with MQTT for Message Broker.",
      "On Site work for the IOT integration with AWS server and data view on Angular.",
      "Implementing responsive design and ensuring cross-browser compatibility.",
    ],
  },
  {
    title: "FullStack Developer",
    company_name: "Astics INC",
    icon: astics,
    iconBg: "#E6DEDD",
    date: "June 2018 - March 2020",
    points: [
      "Developing and maintaining web applications using React.js and other related technologies.",
      "Create server and architectures for full applications with scalable requests in NodeJS.",
      "Implementing responsive design and ensuring cross-browser compatibility.",
      "Build ElectronJS application for Desktop.",
      "Participating in code reviews, team-work, learn and providing constructive feedback to other developers.",
    ],
  },
  {
    title: "FullStack Javascript developer",
    company_name: "Acquaint Softtech Pvt Ltd",
    icon: acquaint,
    iconBg: "#383E56",
    date: "May 2020 - February 2021",
    points: [
      "Developing and maintaining web applications using React.js, NodeJS, ORM, Unity and other related technologies.",
      "Manage Product, handle team and provide better UI/UX and 3D Rendering of model.",
      "Implementation of real time communication though socket.io.",
      "Participating in code reviews, providing constructive feedback to other developers, Scalable on browser with unity.",
    ],
  },
  {
    title: "Freelancer / Contract Developer",
    company_name: "",
    icon: "",
    iconBg: "#E6DEDD",
    date: "March 2021 - Present",
    points: [
      "Develop and Maintain Web Application created on Angular / React, NodeJS, React Native and build CMMS, Procurement, Games and Admin Portal",
      "Deploy applications on AWS with Apache / Nginx.",
      "Payment Integrations.",
      "On-Site visit, Manage Company Teams, Manage Project.",
    ],
  },
];

const testimonials = [
  
];

const projects = [
  {
    name: "Quiz Admin Portal",
    description:
      "Admin portal for Quiz Management developed for client, where participants, users and all game details can be managed.",
    tags: [
      {
        name: "react",
        color: "blue-text-gradient",
      },
      {
        name: "postgresql",
        color: "green-text-gradient",
      },
      {
        name: "nodejs",
        color: "pink-text-gradient",
      },
      {
        name: "paymentgateway",
        color: "green-text-gradient",
      },
    ],
    image: carrent,
  },
  {
    name: "Quiz App",
    description:
      "Mobile application that enables users to play game and win amount by playing quiz and enhance knowledge.",
    tags: [
      {
        name: "react-native",
        color: "blue-text-gradient",
      },
      {
        name: "Axios",
        color: "green-text-gradient",
      },
    ],
    image: jobit,
    play_store_link: "https://play.google.com/store/apps/details?id=com.mindaspirant"
  },
];

export { services, technologies, experiences, testimonials, projects };
